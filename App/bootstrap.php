<?php

include('config.php');
//On initialise le timeZone
ini_set('date.timezone', 'Europe/Paris');

//On ajoute l'autoloader (compatible winwin)
$loader = require_once join(DIRECTORY_SEPARATOR,[dirname(__DIR__), 'vendor', 'autoload.php']);
//dans l'autoloader nous ajoutons notre répertoire applicatif
$loader->addPsr4('App\\',__DIR__);

//Nous instancions un objet Silex\Application
$app = new Silex\Application();

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'dbhost' => hostname,
        'dbname' => database,
        'user' => username,
        'password' => password,
        'charset'   => 'utf8mb4',   // passage en utf8
    ),
));
//utilisation de twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => join(DIRECTORY_SEPARATOR, array(__DIR__, 'Views'))
));
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// utilisation des sessoins
$app->register(new Silex\Provider\SessionServiceProvider());

//en dev, nous voulons voir les erreurs
$app['debug'] = true;

$app->mount("/", new App\Controller\IndexController());
$app->mount("/Spectacle", new App\Controller\SpectacleController($app));
$app->mount("/Theatre", new App\Controller\TheatreController($app));
$app->mount("/connexion", new App\Controller\UserController($app));
//On lance l'application
$app->run();