<?php

namespace App\Controller;

use App\Model\TheatreModel;
use Composer\DependencyResolver\Request;
use Silex\Application;
use Silex\ControllerProviderInterface;
use App\Model\SpectacleModel;

class SpectacleController implements  ControllerProviderInterface
{
    private $spectacleModel;
    private $theatreModel;

    public function index(Application $app){
        return $this->show($app);
    }

    public function show(Application $app){
        $this->spectacleModel = new SpectacleModel($app);
        $spectacles = $this->spectacleModel->getAllSpectacles();
        //var_dump($spectacles);
        return $app["twig"]->render('Spectacle/v_table_spectacle.html.twig',['data'=>$spectacles,'path'=>BASE_URL]);
    }

    public function add(Application $app){
        $this->theatreModel = new TheatreModel($app);
        $theatres = $this->theatreModel->getAllTheatres();
        return $app["twig"]->render('Spectacle/v_form_create_spectacle.html.twig',['theatre'=>$theatres,'path'=>BASE_URL]);
    }
    public function validFormAddSpectacle(Application $app) {
        // var_dump($app['request']->attributes);
        if (isset($_POST['nom_Spectacle']) && isset($_POST['id_theatre']) and isset($_POST['date_representation_spectacle']) and isset($_POST['prix_spectacle'])) {
            $donnees = [
                'nom_Spectacle' => htmlspecialchars($_POST['nom_Spectacle']),                    // echaper les entr�es
                'id_theatre' => htmlspecialchars($_POST['id_theatre']),
                'date_representation_spectacle' => htmlspecialchars($_POST['date_representation_spectacle']),
                'prix_spectacle' => $app->escape($_POST['prix_spectacle'])  //$req->query->get('prix_spectacle')
            ];
            if ((! preg_match("/^[A-Za-z ]{2,}/",$donnees['nom_Spectacle']))) $erreurs['nom_Spectacle']='nom compos� de 2 lettres minimum';
            //if((! preg_match("/^[A-Za-z ]{2,}/",$donnees['date_representation_spectacle']))) $erreurs['date_representation_spectacle']='date de la forme AAAA-MM-JJ';
            if(! is_numeric($donnees['prix_spectacle']))$erreurs['prix_spectacle']='Veuillez saisir une valeur num�rique';

//            $contraintes = new Assert\Collection(
//                [
//                    'nom_Spectacle' => new Assert\Length(array('min' => 10)), //['min'=>2, 'message'=>"Le nom doit faire au moins {{ limit }} caract�res."]
//                    'id_theatre' => new Assert\NotBlank(),
//                    'date_representation_spectacle' => new Assert\Type('float'),
//                    'date_representation_spectacle_spectacle' => new Assert\Length(array('min' => 10)),
//                ]);
//            $errors = $app['validator']->validateValue($donnees,$contraintes);

            if(! empty($erreurs))
            {
                // var_dump($errors);
                // die();
                $this->theatreModel = new TheatreModel($app);
                $theatres = $this->theatreModel->getAllTheatres();
                return $app["twig"]->render('Spectacle/v_form_create_spectacle.html.twig',['donnees'=>$donnees,'erreurs'=>$erreurs,'theatre'=>$theatres,'path'=>BASE_URL]);
            }
            else
            {
                $this->spectacleModel = new SpectacleModel($app);
                $this->spectacleModel->insertSpectacle($donnees);
                return $app->redirect($app["url_generator"]->generate("Spectacle.index"));
            }

        }
        else
            return "error ????? PB data form";

    }
    public function edit(Application $app,$id){
        $this->theatreModel = new TheatreModel($app);
        $theatres = $this->theatreModel->getAllTheatres();

        $this->spectacleModel = new SpectacleModel($app);
        $donnees = $this->spectacleModel->getOneSpectacle($id);
        return $app["twig"]->render('Spectacle/v_form_update_spectacle.html.twig',['theatre'=>$theatres,'donnees'=>$donnees,'path'=>BASE_URL]);
    }
    public function validFormEditSpectacle(Application $app){
        if (isset($_POST['nom_Spectacle']) && isset($_POST['id_theatre']) and isset($_POST['date_representation_spectacle']) and isset($_POST['prix_spectacle'])) {
            $donnees = [
                'id_Spectacle' => htmlspecialchars($_POST['id_Spectacle']),
                'nom_Spectacle' => htmlspecialchars($_POST['nom_Spectacle']),
                'Theatre_id_theatre' => htmlspecialchars($_POST['id_theatre']),
                'date_representation_spectacle' => htmlspecialchars($_POST['date_representation_spectacle']),
                'prix_spectacle' => $app->escape($_POST['prix_spectacle'])  //$req->query->get('prix_spectacle')
            ];
            if ((! preg_match("/^[A-Za-z ]{2,}/",$donnees['nom_Spectacle']))) $erreurs['nom_Spectacle']='nom compos� de 2 lettres minimum';
            //if((! preg_match("/^[A-Za-z ]{2,}/",$donnees['date_representation_spectacle']))) $erreurs['date_representation_spectacle']='date de la forme AAAA-MM-JJ';
            if(! is_numeric($donnees['prix_spectacle']))$erreurs['prix_spectacle']='Veuillez saisir une valeur num�rique';

//            $contraintes = new Assert\Collection(
//                [
//                    'nom_Spectacle' => new Assert\Length(array('min' => 10)), //['min'=>2, 'message'=>"Le nom doit faire au moins {{ limit }} caract�res."]
//                    'id_theatre' => new Assert\NotBlank(),
//                    'date_representation_spectacle' => new Assert\Type('float'),
//                    'date_representation_spectacle_spectacle' => new Assert\Length(array('min' => 10)),
//                ]);
//            $errors = $app['validator']->validateValue($donnees,$contraintes);

            if(! empty($erreurs))
            {
                // var_dump($errors);
                // die();
                $this->theatreModel = new TheatreModel($app);
                $theatres = $this->theatreModel->getAllTheatres();

                $this->spectacleModel = new SpectacleModel($app);
                $donnees = $this->spectacleModel->getOneSpectacle($_POST['id_Spectacle']);

                return $app["twig"]->render('Spectacle/v_form_update_spectacle.html.twig',['donnees'=>$donnees,'erreurs'=>$erreurs,'theatre'=>$theatres,'path'=>BASE_URL]);
            }
            else
            {
                $this->spectacleModel = new SpectacleModel($app);
                $this->spectacleModel->updateSpectacle($donnees);
                return $app->redirect($app["url_generator"]->generate("Spectacle.index"));
            }

        }
        else
            return "error ????? PB data form";
    }

    public function delete(Application $app, $id){
        $this->spectacleModel = new SpectacleModel($app);
        $donnees = $this->spectacleModel->getNomSpectacle($id);
        return $app["twig"]->render('Spectacle/v_form_delete_spectacle.html.twig',['donnees'=>$donnees,'path'=>BASE_URL]);
    }

    public function validFormDeleteSpectacle(Application $app){
        $this->spectacleModel = new SpectacleModel($app);
        $this->spectacleModel->deleteSpectacle($_POST['id_Spectacle']);
        return $app->redirect($app["url_generator"]->generate("Spectacle.index"));
    }

    public function connect(Application $app) {  //http://silex.sensiolabs.org/doc/providers.html#controller-providers
        $controllers = $app['controllers_factory'];

        $controllers->get('/', 'App\Controller\SpectacleController::index')->bind('Spectacle.index');
        $controllers->get('/show', 'App\Controller\SpectacleController::show')->bind('Spectacle.show');

        $controllers->get('/add', 'App\Controller\SpectacleController::add')->bind('Spectacle.add');
        $controllers->post('/add', 'App\Controller\SpectacleController::validFormAddSpectacle')->bind('Spectacle.validFormAddSpectacle');

        $controllers->match('/delete/{id}', 'App\Controller\SpectacleController::delete')->bind('Spectacle.delete');
        $controllers->post('/delete/', 'App\Controller\SpectacleController::validFormDeleteSpectacle')->bind('Spectacle.validFormDeleteSpectacle');

        $controllers->match('/edit/{id}', 'App\Controller\SpectacleController::edit')->bind('Spectacle.edit');
        $controllers->post('/edit/', 'App\Controller\SpectacleController::validFormEditSpectacle')->bind('Spectacle.validFormEditSpectacle');
        return $controllers;
    }
}