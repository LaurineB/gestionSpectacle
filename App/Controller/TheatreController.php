<?php
namespace App\Controller;

use App\Model\TheatreModel;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;


class TheatreController implements  ControllerProviderInterface
{
    private $theatreModel;

    public function index(Application $app)
    {
        return $this->show($app);
    }

    public function show(Application $app)
    {
        $this->theatreModel = new TheatreModel($app);
        $theatres = $this->theatreModel->getAllTheatres();
        return $app["twig"]->render('Theatre/v_table_theatre.html.twig', ['data' => $theatres, 'path' => BASE_URL]);
    }

    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->get('/', 'App\Controller\TheatreController::index')->bind('Theatre.index');
        $controllers->get('/show', 'App\Controller\TheatreController::show')->bind('Theatre.show');


        return $controllers;
    }
}