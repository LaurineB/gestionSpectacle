<?php
/**
 * Created by PhpStorm.
 * User: labai
 * Date: 20/01/2016
 * Time: 13:44
 */

namespace App\Model;


use Doctrine\DBAL\Query\QueryBuilder;
use Silex\Application;

class TheatreModel
{
    private $db;

    public function __construct(Application $app){
        $this->db = $app['db'];
    }
    function getAllTheatres(){
        $queryBuilder = new QueryBuilder($this->db);
        $queryBuilder
            ->select('id_Theatre', 'nom_Theatre','adr_theatre','tel_theatre')
            ->from('theatre');
        return $queryBuilder->execute()->fetchAll();
    }
}