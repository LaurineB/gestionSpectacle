<?php
namespace App\Model;
use Doctrine\DBAL\Query\QueryBuilder;
use Silex\Application;

class SpectacleModel
{
    private $db;

    public function __construct(Application $app){
        $this->db = $app['db'];
    }

    public function getAllSpectacles(){
        $queryBuilder = new QueryBuilder($this->db);
        $queryBuilder
            ->select('id_Spectacle', 'nom_Spectacle', 'date_representation_spectacle', 'prix_spectacle','id_theatre', 'nom_theatre')
            ->from('spectacle')
            ->join('spectacle', 'Theatre', 't', 'spectacle.Theatre_id_Theatre=t.id_Theatre')
            ->orderBy('id_Spectacle');
        return $queryBuilder->execute()->fetchAll();
    }
    public function getOneSpectacle($id){
        $queryBuilder = new QueryBuilder($this->db);
        $queryBuilder
            ->select('id_Spectacle', 'nom_Spectacle', 'date_representation_spectacle', 'prix_spectacle','id_Theatre','nom_theatre')
            ->from('spectacle')
            ->join('spectacle', 'Theatre', 't', 'spectacle.Theatre_id_Theatre=t.id_Theatre')
            ->where('id_Spectacle = ?')
            ->setParameter(0,$id);
        return $queryBuilder->execute()->fetch();
    }
    public function getNomSpectacle($id){
        $queryBuilder = new QueryBuilder($this->db);
        $queryBuilder
            ->select('id_Spectacle','nom_Spectacle')
            ->from('spectacle')
            ->where('id_Spectacle = ?')
            ->setParameter(0,$id);
        return $queryBuilder->execute()->fetch();
    }
    public function insertSpectacle($donnees){
        $queryBuilder = new QueryBuilder($this->db);
        $queryBuilder->insert('spectacle')
            ->values([
                'id_Spectacle' => 'default',
                'nom_Spectacle' => '?',
                'date_representation_spectacle' => '?',
                'prix_spectacle' => '?',
                'Theatre_id_Theatre' => '?'
            ])
            ->setParameter(0, $donnees['nom_Spectacle'])
            ->setParameter(1, $donnees['date_representation_spectacle'])
            ->setParameter(2, $donnees['prix_spectacle'])
            ->setParameter(3, $donnees['id_theatre'])
        ;
        return $queryBuilder->execute();
    }
    public function deleteSpectacle($id_spectacle){
        $queryBuilder = new QueryBuilder($this->db);
        $queryBuilder->delete('spectacle')
            ->where('id_Spectacle = ?')
            ->setParameter(0, $id_spectacle);
        return $queryBuilder->execute();
    }
    public function updateSpectacle($donnees){
        $queryBuilder = new QueryBuilder($this->db);
        $queryBuilder->update('spectacle')
            ->set('nom_Spectacle','?')
            ->set('date_representation_spectacle', '?')
            ->set('prix_spectacle','?')
            ->set('Theatre_id_Theatre','?')
            ->where('id_Spectacle = ?')

            ->setParameter(0, $donnees['nom_Spectacle'])
            ->setParameter(1, $donnees['date_representation_spectacle'])
            ->setParameter(2, $donnees['prix_spectacle'])
            ->setParameter(3, $donnees['Theatre_id_theatre'])
            ->setParameter(4, $donnees['id_Spectacle']);
        return $queryBuilder->execute();
    }
}